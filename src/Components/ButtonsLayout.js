import React from "react";
import ResetButton from "./ResetButton";
import StartButton from "./StartButton";
import StopButton from "./StopButton";

export default function ButtonsLayout({ handleReset, handleStart, handleStop }) {
  return(
    <div className="buttons-layout">
      <ResetButton handleReset = {handleReset} />
      <StartButton handleStart = {handleStart} />
      <StopButton handleStop = {handleStop} />
    </div>
  )
}