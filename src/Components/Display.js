import React from "react";

export default function Display({hour, min, sec}) {
  return (
    <div className="display">
      <h1>{hour} : {min} : {sec}</h1>
      <p>JAM : MENIT : DETIK</p>
    </div>
  )
}