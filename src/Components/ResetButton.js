import React from 'react';

export default function ResetButton({ handleReset }) {
  return (
    <button type="button" className='btn reset' onClick={() => handleReset()}>
      Reset
    </button>
  )
}