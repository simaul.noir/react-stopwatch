import React from 'react';

export default function StartButton({ handleStart }) {
  return(
    <button type="button" className='btn start' onClick={() => handleStart()}>
      Start
    </button>
  )
}