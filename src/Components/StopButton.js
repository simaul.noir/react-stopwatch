import React from "react";

export default function StopButton({ handleStop }) {
  return(
    <button tpye="button" className="btn stop" onClick={() => handleStop()}>
      Stop
    </button>
  )
}