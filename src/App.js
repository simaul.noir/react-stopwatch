              import React, { useState, useEffect } from 'react';
import Display from './Components/Display';
import ButtonsLayout from './Components/ButtonsLayout';
import './App.css';

function App() {
  const [time, setTime] = useState(0);
  const [isRun, setIsRun] = useState(false);

  useEffect(() => {
    let timer;
    if (isRun) {
      timer = setInterval(() => setTime(time + 1), 1000);
    }
    return () => clearInterval(timer);
  },[isRun, time])

  // Hours calculation
  const hours = Math.floor(time / 3600);

  // Minutes calculation
  const minutes = Math.floor((time % 3600) / 60);

  // Seconds calculation
  const seconds = Math.floor((time % 60));

  const handleStart = () => {
    setIsRun(true);
  }

  const handleStop = () => {
    setIsRun(false);
  }

  const handleReset = () => {
    setTime(0)
  }

  return (
    <div className='container'>
      <div className='display-container'>
        <Display hour={hours} min={minutes} sec={seconds} />
      </div>
      <ButtonsLayout handleStart={handleStart} handleReset={handleReset} handleStop={handleStop} />
    </div>
  )
}

export default App;
